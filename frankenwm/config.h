/* see LICENSE for copyright and license */

#ifndef CONFIG_H
#define CONFIG_H

/* Button definitions, nothing to edit for you */
#define MOD1            Mod1Mask    /* ALT key */
#define MOD4            Mod4Mask    /* Super/Windows key */
#define CONTROL         ControlMask /* Control key */
#define SHIFT           ShiftMask   /* Shift key */

/* EDIT THIS: general settings */
#define MASTER_SIZE     0.5       /* master-stack ratio */
#define SHOW_PANEL      False     /* show panel by default on exec */
#define TOP_PANEL       True      /* False means panel is on bottom */
#define PANEL_HEIGHT    0         /* 0 for no space for panel, thus no panel */
#define DEFAULT_MODE    FIBONACCI      /* TILE MONOCLE BSTACK GRID FIBONACCI EQUAL */
#define ATTACH_ASIDE    True      /* False means new window is master */
#define FOLLOW_MOUSE    True      /* Focus the window the mouse just entered */
#define FOLLOW_WINDOW   False     /* Follow the window when moved to a different desktop */
#define CLICK_TO_FOCUS  True      /* Focus an unfocused window when clicked */
#define BORDER_WIDTH    3         /* window border width */
#define SCRATCH_WIDTH   0         /* scratch window border width, 0 to disable */
#define FOCUS           "#666666" /* focused window border color   */
#define UNFOCUS         "#CECECE" /* unfocused window border color */
#define SCRATCH         "#cc0000" /* scratchpad border color */
#define DESKTOPS        5         /* number of desktops - edit DESKTOPCHANGE keys to suit */
#define DEFAULT_DESKTOP 0         /* the desktop to focus on exec */
#define MINWSZ          50        /* minimum window size in pixels */
#define USELESSGAP      10        /* the size of the useless gap in pixels */
#define GLOBALGAPS      True      /* use the same gap size on all desktops */
#define MONOCLE_BORDERS True      /* display borders in monocle mode */
#define INVERT          False     /* use alternative modes by default */
#define AUTOCENTER      False     /* automatically center windows floating by default */
#define OUTPUT          False     /* write desktop info to stdout */
#define OUTPUT_TITLE    False     /* output the title of the currently active window */
#define USE_SCRATCHPAD  False     /* enable the scratchpad functionality */
#define CLOSE_SCRATCHPAD True     /* close scratchpad on quit */
#define SCRPDNAME       "pad"	  /* the instance of the scratchpad window */
#define EWMH_TASKBAR    False     /* False if text (or no) panel/taskbar */

/*
 * EDIT THIS: applicaton specific rules
 * Open applications to specified desktop with specified mode.
 * If desktop is negative, then current is assumed. Desktops are 0-indexed.
 * If border_width is negative, the default is assumed.
 *
 * The matching is done via POSIX-ERE-regexes on the class or instance strings
 * as reported by xprop(1):
 *
 *     WM_CLASS(STRING) = instance, class
 *
 * See https://en.wikipedia.org/wiki/Regular_expression#POSIX_extended for regex
 * syntax (hint: ".*" matches any string, "^GIMP$" only matches "GIMP", but an
 * unanchored "GIMP" also matches "AGIMPE").
 *
 * Sadly, this can not be empty (for now), so enter something non-existent if
 * you do not wish to use this functionality.
 */
static const AppRule rules[] = { \
    /* class    desktop  follow  float  border_with */
    { "rxvt",	   -1,      False,  False,   3},
    { "term",	   -1,      False,  True,    3},
    { "float",	   -1,      False,  True,    3},
    { "pulsemixer",-1,      False,  True,    3},
    { "firefox",   -1,      True,   False,   3},
};

/* helper for spawning shell commands, usually you don't edit this */
#define SHCMD(cmd) {.com = (const char*[]){"/bin/sh", "-c", cmd, NULL}}

/*
 * EDIT THIS: commands
 * Adjust and add these to the shortcuts below to launch anything you want by
 * pressing a key (combination). The last argument should ALWAYS be a null
 * pointer. scrpcmd needs to be defined and different from all other commands
 * (like the example) so FrankenWM can tell when you want to open a scratchpad
 * window. The title of the scratchpad window should also match SCRPDNAME from
 * above
 */
static const char *dmenucmd[] = { "dmenu_run", "-b", "-x", "560", "-y", "20", "-z", "700", "-p", "VisoneRun:",  NULL }; 
static const char *launchercmd[] = {"launcher", NULL };
static const char *fzfmenucmd[] = { "term-launcher",  "-m", NULL };

/* the st/urxvt terminal*/
static const char *scratchpadcmd[] = {"sctpad", "float", NULL }; 
static const char *termcmd[]  = {"sctpad", "scratchpad",  NULL };
static const char *urxcmd[] = {"sctpad", "rxvt", NULL };
static const char *urxpmcmd[] = {"sctpad", "pulsemixer", NULL };
static const char *scrpcmd[] = { "xterm", "-T", "scratchpad", NULL };

/* Apps bindings */
static const char *ffcmd[]          = { "sctpad", "firefox", NULL }; 


/* Audio */
#include<X11/XF86keysym.h>

static const char *upvolcmd[]    = { "dwm-vol", "+", NULL };
static const char *downvolcmd[]  = { "dwm-vol", "-", NULL };
static const char *mutevolcmd[]  = { "dwm-vol", "m", NULL };


/* Brightness */

static const char *brightpcmd[] =  { "bright", "+", NULL };
static const char *brightmcmd[] =  { "bright", "-", NULL };


/* Visone Scripts*/
static const char *sessioncmd[]  = { "void-session", NULL };
static const char *fzfilmscmd[]  = { "term-launcher", "-f", NULL };
static const char *fztvshowcmd[] = { "term-launcher", "-t", NULL };
static const char *fzvarioscmd[] = { "term-launcher", "-v", NULL };
static const char *ssmcmd[]       = { "dwm-screenshoots", "-m", NULL };
static const char *sscmd[]       = { "dwm-screenshoots", "-s", NULL };


#define DESKTOPCHANGE(K,N) \
    {  MOD4,             K,              change_desktop, {.i = N}}, \
    {  MOD4|ShiftMask,   K,              client_to_desktop, {.i = N}},

/*
 * EDIT THIS: shortcuts
 * By default, all shortcuts use only Mod4 (+ Shift/Control), but you can use
 * Mod1 as well if you like to, I just prefer not to. (Update: handling
 * floating windows makes more sense when using Mod1 as well, so there's that)
 */
static key keys[] = {
    /* modifier          key            function           argument */


	{ MOD4,				XK_d,      spawn,          {.com = dmenucmd}}, // dmenu
	{ MOD1|SHIFT,			XK_d,      spawn,          {.com = fzfmenucmd}}, // fzf-menu
	{ MOD1,				XK_d, 	   spawn,  	   {.com = launchercmd}}, // launcher
	{ MOD4,            	        XK_Return, spawn, 	   {.com = urxcmd}}, // urxvtc st/nnn
	{ MOD4|SHIFT,            	XK_Return, spawn,  	   {.com = termcmd}}, // st/nnn tabbed
	{ MOD4,            		XK_p, 	   spawn,  	   {.com = urxpmcmd}}, // urxvtc pulsemixer
	{ MOD1,				XK_Return, spawn,  	   {.com = scratchpadcmd}}, // urxvtc float
	{ MOD4,  	      		XK_b,      spawn,  	   {.com = ffcmd}}, //ff browser
	{ 0,                  		XK_Print,  spawn,          {.com = sscmd}}, //desktop-screenshot
	{ MOD4,             		XK_Print,  spawn,          {.com = ssmcmd}}, //menu-screenshot
	{ MOD4,           		XK_s,      spawn,          {.com = sessioncmd}}, //session

		/* Audio Bindings */

	{ 0, XF86XK_AudioMute,        	spawn,      {.com = mutevolcmd}}, // audio mute
	{ 0, XF86XK_AudioLowerVolume,   spawn,      {.com = downvolcmd}}, // audio down
	{ 0, XF86XK_AudioRaiseVolume,   spawn,      {.com = upvolcmd}}, // audio plus
	

		/* Brightness bindings */

	{ 0, XF86XK_MonBrightnessUp,	spawn,	{.com = brightpcmd}},
	{ 0, XF86XK_MonBrightnessDown,	spawn,	{.com = brightmcmd}},



		/* Scripts Alt + Key */

	{MOD1,				XK_1,	spawn,		{.com = fzfilmscmd   } },		
	{MOD1,				XK_2,   spawn,		{.com = fztvshowcmd   } },		
	{MOD1,				XK_3,   spawn,		{.com = fzvarioscmd   } },		



    /* select windows */
    {  MOD4,				XK_Tab,			next_win,	{NULL}},
    {  MOD4|SHIFT,			XK_Tab,			prev_win,	{NULL}},
    /* select the master window, or the previously focussed slave */
    {  MOD4,				XK_a,			focusmaster,	{NULL}},
    /* select urgent window */
    {  MOD4|SHIFT,			XK_a,			focusurgent,    {NULL}},

    /* move windows */
    {  MOD4|SHIFT,			XK_Down,		move_down,      {NULL}},
    {  MOD4|SHIFT,			XK_Up,			move_up,        {NULL}},
    /* swap the current window to master */
    {  MOD4,				XK_z,			swap_master,    {NULL}},
    /* maximize the current window */
    {  MOD4,				XK_f,			maximize,       {NULL}},
    /* minimize window to queue/pull window from queue */
    {  MOD1,				XK_y,			minimize,       {NULL}},
    {  MOD1,				XK_u,			restore,        {NULL}},
    /* move the current window to the center of the screen, floating */
    {  MOD4,				XK_c,			centerwindow,   {NULL}}, 
    /* toggles inverted stacking modes (left/top stack) */
    {  MOD1,				XK_0,			invertstack,    {NULL}},
    /* show/hide all windows on all desktops */
    {  MOD1|CONTROL,			XK_h,			showhide,       {NULL}},
    /* toggle the scratchpad terminal, if enabled */
    {  MOD4,				XK_F12,			togglescratchpad,  {NULL}},

    /* move floating windows */
    {  MOD1,				XK_Down,		float_y,        {.i = +10}},
    {  MOD1,				XK_Up,			float_y,        {.i = -10}},
    {  MOD1,				XK_Left,		float_x,        {.i = -10}},
    {  MOD1,				XK_Right,		float_x,        {.i = +10}},
    /* resize floating windows */
    {  MOD1|CONTROL,			XK_Down,		resize_y,       {.i = +10}},
    {  MOD1|CONTROL,			XK_Up,			resize_y,       {.i = -10}},
    {  MOD1|CONTROL,			XK_Left,		resize_x,       {.i = -10}},
    {  MOD1|CONTROL,			XK_Right,		resize_x,       {.i = +10}},
    /* reset the selected floating window to tiling */
    {  MOD4,				XK_space,		tilemize,       {NULL}},

    /* mode selection */
    {  CONTROL,				XK_1,			switch_mode,    {.i = TILE}},
    {  CONTROL,				XK_2,          switch_mode,		{.i = MONOCLE}},
  /*  {  CONTROL,			XK_b,          switch_mode,		{.i = BSTACK}}, */
  /*  {  CONTROL,			XK_g,          switch_mode,		{.i = GRID}}, */
    {  CONTROL,				XK_3,          switch_mode,		{.i = FIBONACCI}},
 /*   {  CONTROL,			XK_d,          switch_mode,		{.i = DUALSTACK}},*/
 /*   {  CONTROL,			XK_e,          switch_mode,		{.i = EQUAL}}, */
    {  CONTROL,				XK_minus,      rotate_mode,		{.i = -1}},
    {  CONTROL,				XK_plus,       rotate_mode,		{.i = +1}},

    /* kill current window */
    {  MOD4,				XK_q,          killclient,		{NULL}},

    /* desktop selection */
       DESKTOPCHANGE(    XK_1,                             0)
       DESKTOPCHANGE(    XK_2,                             1)
       DESKTOPCHANGE(    XK_3,                             2)
       DESKTOPCHANGE(    XK_4,                             3)
       DESKTOPCHANGE(    XK_5,                             4)

    /* toggle to last desktop */
    {  MOD4|CONTROL,			XK_Tab,        last_desktop,		{NULL}},
    /* jump to the next/previous desktop */
    {  MOD4|CONTROL,			XK_Left,	rotate,			{.i = -1}},
    {  MOD4|CONTROL,			XK_Right,       rotate,			{.i = +1}},
    /* jump to the next/previous desktop with just the current window */
    {  MOD4|CONTROL|SHIFT,		XK_Left,        rotate_client,		{.i = -1}},
    {  MOD4|CONTROL|SHIFT,		XK_Right,       rotate_client,		{.i = +1}},
    /* jump to the next/previous desktop with all windows */
    {  CONTROL,				XK_Left, 	rotate_filled,		{.i = -1}},
    {  CONTROL,				XK_Right,       rotate_filled,		{.i = +1}},

    /* resize master/first stack window */
    {  MOD4,				XK_h,          resize_master,		{.i = -10}},
    {  MOD4,				XK_l,          resize_master,		{.i = +10}},
    {  MOD4,				XK_o,          resize_stack,		{.i = -10}},
    {  MOD4,				XK_i,          resize_stack,		{.i = +10}},

    /* resize the borders */
    {  MOD4|CONTROL,			XK_minus,       adjust_borders,		{.i = -1}},
    {  MOD4|CONTROL,			XK_plus,        adjust_borders,		{.i = +1}},
    /* resize the useless gaps between the windows */
    {  MOD4,				XK_minus,       adjust_gaps,		{.i = -1}},
    {  MOD4,				XK_plus,        adjust_gaps,		{.i = +1}},
    /* toggle the panel space */
    {  MOD4|CONTROL,			XK_9,          togglepanel,		{NULL}},

    /* exit */
    {  MOD4|SHIFT,			XK_q,          quit,              {.i = 0}},
};

/* EDIT THIS: mouse-based shortcuts */
static Button buttons[] = {
    /* move/resize using the mouse */
    {  MOD4,    Button1,     mousemotion,   {.i = MOVE}},
    {  MOD4,    Button3,     mousemotion,   {.i = RESIZE}},
};
#endif
