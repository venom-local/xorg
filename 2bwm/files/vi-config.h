///---User configurable stuff---///
///---Modifiers---///
#define MOD             XCB_MOD_MASK_4       /* Super/Windows key  or check xmodmap(1) with -pm  defined in /usr/include/xcb/xproto.h */
///--Speed---///
/* Move this many pixels when moving or resizing with keyboard unless the window has hints saying otherwise.
 *0)move step slow   1)move step fast
 *2)mouse slow       3)mouse fast     */
static const uint16_t movements[] = {20,40,15,400};
/* resize by line like in mcwm -- jmbi */
static const bool     resize_by_line          = true;
/* the ratio used when resizing and keeping the aspect */
static const float    resize_keep_aspect_ratio= 1.03;
///---Offsets---///
/*0)offsetx          1)offsety
 *2)maxwidth         3)maxheight */
static const uint8_t offsets[] = {0,0,0,0};
///---Colors---///
/*0)focuscol         1)unfocuscol
 *2)fixedcol         3)unkilcol
 *4)fixedunkilcol    5)outerbordercol
 *6)emptycol         */
static const char *colors[] = {"#8A8A8A","#D9D9D9","#444444","#D9D9D9","#040405","#676C76","#D9D9D9"};
/* if this is set to true the inner border and outer borders colors will be swapped */
static const bool inverted_colors = true;
///---Cursor---///
/* default position of the cursor:
 * correct values are:
 * TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT, MIDDLE
 * All these are relative to the current window. */
#define CURSOR_POSITION MIDDLE
///---Borders---///
/*0) Outer border size. If you put this negative it will be a square.
 *1) Full borderwidth    2) Magnet border size
 *3) Resize border size  */
static const uint8_t borders[] = {3,5,5,4};
/* Windows that won't have a border.
 * It uses substring comparison with what is found in the WM_NAME
 * attribute of the window. You can test this using `xprop WM_NAME`
 */
#define LOOK_INTO "WM_NAME"
static const char *ignore_names[] = {"bar", "xclock"};
///--Menus and Programs---///
static const char *menucmd[] = { "dmenu_run", "-b", "-x", "560", "-y", "20", "-z", "700", "-p", "VisoneRun:",  NULL }; 
static const char *launchercmd[] = {"launcher", NULL };
static const char *fzfmenucmd[] = { "term-launcher",  "-m", NULL };
static const char *scratchpadcmd[] = {"sctpad", "float", NULL }; 
static const char *termcmd[]  = {"sctpad", "scratchpad",  NULL };
static const char *urxcmd[] = {"sctpad", "rxvt", NULL };
static const char *urxpmcmd[] = {"sctpad", "vi-tg", NULL };
static const char *browsercmd[] = { "sctpad", "vi-brave", NULL }; 
static const char *sessioncmd[]  = { "void-session", NULL };
static const char *filmcmd[]  = { "term-launcher", "-f", NULL };
static const char *showcmd[] = { "term-launcher", "-t", NULL };
static const char *variocmd[] = { "term-launcher", "-v", NULL };
static const char *mcapcmd[]       = { "dwm-screenshoots", "-m", NULL };
static const char *scapcmd[]       = { "dwm-screenshoots", "-s", NULL };
static const char *upcmd[]    = { "dwm-vol", "+", NULL };
static const char *downcmd[]  = { "dwm-vol", "-", NULL };
static const char *mutecmd[]  = { "dwm-vol", "m", NULL };

///--Custom foo---///
static void halfandcentered(const Arg *arg)
{
	Arg arg2 = {.i=TWOBWM_MAXHALF_VERTICAL_LEFT};
	maxhalf(&arg2);
	Arg arg3 = {.i=TWOBWM_TELEPORT_CENTER};
	teleport(&arg3);
}

#include <X11/XF86keysym.h>
#define DESKTOPCHANGE(K,N) \
{  MOD ,             K,              changeworkspace, {.i=N}}, \
{  MOD |SHIFT,       K,              sendtoworkspace, {.i=N}},
static key keys[] = {
    /* modifier           key            function           argument */

	    // Focus to next/previous window
    {  MOD ,              XK_Tab,        focusnext,         {.i=TWOBWM_FOCUS_NEXT}},
    {  MOD |SHIFT,        XK_Tab,        focusnext,         {.i=TWOBWM_FOCUS_PREVIOUS}},
	    // Kill a window
    {  MOD ,              XK_q,          deletewin,         {}},

	    // Resize a window
    {  ALT ,		  XK_Up,          resizestep,        {.i=TWOBWM_RESIZE_UP}},
    {  ALT ,		  XK_Down,        resizestep,        {.i=TWOBWM_RESIZE_DOWN}},
    {  ALT ,		  XK_Right,       resizestep,        {.i=TWOBWM_RESIZE_RIGHT}},
    {  ALT ,		  XK_Left,        resizestep,        {.i=TWOBWM_RESIZE_LEFT}},

	    // Move a window
    {  ALT |CONTROL,        XK_Up,          movestep,          {.i=TWOBWM_MOVE_UP}},
    {  ALT |CONTROL,        XK_Down,        movestep,          {.i=TWOBWM_MOVE_DOWN}},
    {  ALT |CONTROL,        XK_Right,       movestep,          {.i=TWOBWM_MOVE_RIGHT}},
    {  ALT |CONTROL,        XK_Left,        movestep,          {.i=TWOBWM_MOVE_LEFT}},

	    // Teleport the window to an area of the screen.
	    // Center:
    {  ALT ,              XK_c,          teleport,          {.i=TWOBWM_TELEPORT_CENTER}},

	    // Center y:
    {  ALT |SHIFT,        XK_c,          teleport,          {.i=TWOBWM_TELEPORT_CENTER_Y}},

	    // Center x:
    {  ALT |CONTROL,      XK_c,          teleport,          {.i=TWOBWM_TELEPORT_CENTER_X}},

	    // Top left:
    {  ALT |SHIFT ,       XK_Left,       teleport,          {.i=TWOBWM_TELEPORT_TOP_LEFT}},

	    // Top right:
    {  ALT |SHIFT,        XK_Right,      teleport,          {.i=TWOBWM_TELEPORT_TOP_RIGHT}},

	    // Bottom left:
    {  ALT |CONTROL ,     XK_Left,       teleport,          {.i=TWOBWM_TELEPORT_BOTTOM_LEFT}},

	    // Bottom right:
    {  ALT |CONTROL ,	  XK_Right,      teleport,          {.i=TWOBWM_TELEPORT_BOTTOM_RIGHT}},

	    // Resize while keeping the window aspect
    {  MOD ,              XK_Home,       resizestep_aspect, {.i=TWOBWM_RESIZE_KEEP_ASPECT_GROW}},
    {  MOD ,              XK_End,        resizestep_aspect, {.i=TWOBWM_RESIZE_KEEP_ASPECT_SHRINK}},

	    // Maximize (ignore offset and no EWMH atom)
    {  MOD ,              XK_x,          maximize,          {}},

	    // Full screen (disregarding offsets and adding EWMH atom)
    {  MOD |SHIFT ,       XK_x,          fullscreen,        {}},

	    // Maximize vertically
    {  ALT ,              XK_v,          maxvert_hor,       {.i=TWOBWM_MAXIMIZE_VERTICALLY}},

	    // Maximize horizontally
    {  ALT ,		  XK_h,          maxvert_hor,       {.i=TWOBWM_MAXIMIZE_HORIZONTALLY}},

	    // Maximize and move
	    // vertically left
    {  MOD ,		  XK_Left,       maxhalf,           {.i=TWOBWM_MAXHALF_VERTICAL_LEFT}},

	    // vertically right
    {  MOD ,		  XK_Right,      maxhalf,           {.i=TWOBWM_MAXHALF_VERTICAL_RIGHT}},

	    // horizontally left
    {  MOD ,		  XK_Down,       maxhalf,           {.i=TWOBWM_MAXHALF_HORIZONTAL_BOTTOM}},

	    // horizontally right
    {  MOD ,		  XK_Up,         maxhalf,           {.i=TWOBWM_MAXHALF_HORIZONTAL_TOP}},

	    // Next/Previous workspace
    {  CONTROL ,          XK_Right,      nextworkspace,     {}},
    {  CONTROL ,          XK_Left,       prevworkspace,     {}},

	    // Move to Next/Previous workspace
    {  CONTROL |SHIFT ,   XK_Up,	 sendtonextworkspace,{}},
    {  CONTROL |SHIFT ,   XK_Down,       sendtoprevworkspace,{}},

	     // Iconify the window
    {  MOD ,              XK_i,          hide,              {}},

	    // Start visone
    { MOD,		  XK_d,          start,             {.com = menucmd}}, // dmenu
    { MOD|SHIFT,          XK_d,		 start,		    {.com = fzfmenucmd}}, // fzf-menu
    { ALT,             	  XK_d, 	 start,  	    {.com = launchercmd}}, // launcher
    { MOD,            	  XK_Return,	 start, 	    {.com = urxcmd}}, // urxvtc st/nnn
    { MOD |SHIFT,         XK_Return,	 start,  	    {.com = termcmd}}, // st/nnn tabbed
    { MOD,            	  XK_p, 	 start,  	    {.com = tgcmd}}, // telegram
    { ALT,             	  XK_Return,	 start,  	    {.com = scratchpadcmd}}, // urxvtc float
    { MOD,  	      	  XK_f,		 start,  	    {.com = browsercmd}}, // browser
    { 0,                  XK_Print,	 start,		    {.com = scapcmd}}, //desktop-screenshot
    { MOD,             	  XK_Print,	 start,             {.com = mcapcmd}}, //menu-screenshot
    { ALT,           	  XK_s,		 start,		    {.com = sessioncmd}}, //session
    { ALT,		  XK_1,		 start,		    {.com = filmcmd}},		
    { ALT,		  XK_2,		 start,		    {.com = showcmd}},		
    { ALT,		  XK_3,		 start,		    {.com = variocmd}},		

	    // Audio Bindings 
    { 0, XF86XK_AudioMute,        	start,		    {.com = mutecmd}}, // audio mute
    { 0, XF86XK_AudioLowerVolume,	start,		    {.com = downcmd}}, // audio down
    { 0, XF86XK_AudioRaiseVolume,	start,		    {.com = upcmd}}, // audio plus

	    // Exit or restart 2bwm
    {  MOD |SHIFT,        XK_q,          twobwm_exit,       {.i=0}},
    {  MOD |SHIFT,        XK_r,          twobwm_restart,    {.i=0}},
    {  MOD ,              XK_space,      halfandcentered,   {.i=0}},
   
	    // Change current workspace
       DESKTOPCHANGE(     XK_1,                             0)
       DESKTOPCHANGE(     XK_2,                             1)
       DESKTOPCHANGE(     XK_3,                             2)
       DESKTOPCHANGE(     XK_4,                             3)
       DESKTOPCHANGE(     XK_5,                             4)
};
// the last argument makes it a root window only event
static Button buttons[] = {
    {  MOD        ,XCB_BUTTON_INDEX_1,     mousemotion,   {.i=TWOBWM_MOVE}, false},
    {  MOD        ,XCB_BUTTON_INDEX_3,     mousemotion,   {.i=TWOBWM_RESIZE}, false},
    {  0          ,XCB_BUTTON_INDEX_3,     start,         {.com = menucmd}, true},
    {  MOD|SHIFT,  XCB_BUTTON_INDEX_1,     changeworkspace, {.i=0}, false},
    {  MOD|SHIFT,  XCB_BUTTON_INDEX_3,     changeworkspace, {.i=1}, false},
    {  MOD|ALT,    XCB_BUTTON_INDEX_1,     changescreen,    {.i=1}, false},
    {  MOD|ALT,    XCB_BUTTON_INDEX_3,     changescreen,    {.i=0}, false}
};
