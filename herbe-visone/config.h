static const char *background_color = "#2E3440";
static const char *border_color = "#B48EAD";
static const char *font_color = "#81A1C1";
static const char *font_pattern = "FantasqueSansMono Nerd Font Mono:style=Regular:size=14";
static unsigned line_spacing = 5;
static unsigned int padding = 15;

static unsigned int width = 450;
static unsigned int border_size = 3;
static unsigned int pos_x = 30;
static unsigned int pos_y = 60;

enum corners { TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT };
enum corners corner = TOP_RIGHT;

static unsigned int duration = 3; /* in seconds */

#define DISMISS_BUTTON Button1
#define ACTION_BUTTON Button3
